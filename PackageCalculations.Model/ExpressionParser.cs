﻿namespace PackageCalculations.Model
{
    using System;
    using System.Threading;
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Operations;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;
    using PackageCalculations.Model.Statistics;

    internal sealed class ExpressionParser : IExpressionParser
    {
        readonly string expression;
        private readonly IFactory<IReader, IOperand> operandsFactory;
        private readonly CancellationTokenSource cancellationSource;
        private readonly IFactory<IOperationsStack> operationsStackFactoy;
        private readonly IFactory<char, bool, IStatisticsReporter, IOperation> operationsFactory;
        private readonly IStreamGenerator streamGenerator;
        private readonly IStatisticsReporter statisticsReporter;

        public ExpressionParser(string expression,
            IFactory<IReader, IOperand> operandsFactory,
            IFactory<IOperationsStack> operationsStackFactoy,
            IFactory<char, bool, IStatisticsReporter, IOperation> operationsFactory,
            IStreamGenerator streamGenerator,
            IStatisticsReporter statisticsReporter)
        {
            this.expression = expression;
            this.operandsFactory = operandsFactory;
            this.operationsStackFactoy = operationsStackFactoy;
            this.operationsFactory = operationsFactory;
            this.streamGenerator = streamGenerator;
            this.statisticsReporter = statisticsReporter;

            cancellationSource = new CancellationTokenSource();
        }

        public IOperand Parse()
        {
            try
            {
                IOperand result;
                using (var reader = streamGenerator.GetBinaryReader(expression))
                {
                    result = ParseExpression(reader, '\n');
                }

                return result;
            }
            catch (FormatException)
            {
                return null;
            }
            catch (DivideByZeroException)
            {
                return null;
            }
            catch (OverflowException)
            {
                return null;
            }
        }

        private IOperand ParseExpression(IReader reader, char end)
        {
            char ch;
            var operationsStack = operationsStackFactoy.Create();
            do
            {
                cancellationSource.Token.ThrowIfCancellationRequested();
                ch = (char)reader.PeekChar();

                IOperand operand;
                if (Char.IsNumber(ch))
                {
                    operand = operandsFactory.Create(reader);
                    operationsStack.PushOperand(operand);
                    continue;
                }

                reader.ReadChar();
                if (ch.Equals(' '))
                {
                    continue;
                }

                if (ch.Equals('('))
                {
                    operand = ParseExpression(reader, ')');
                    operationsStack.PushOperand(operand);
                    continue;
                }

                var currentOperation = operationsFactory.Create(ch, operationsStack.CurNumber == null, statisticsReporter);

                if (currentOperation.Priority > 0)
                {
                    operationsStack.PushOperation(currentOperation);
                    continue;
                }

                if (!ch.Equals(end))
                    throw new FormatException(string.Format("Operation is not supported {0}", ch));

                operationsStack.ExecuteAll();

            } while (!ch.Equals(end));

            return operationsStack.PrevNumber;
        }

        public void Cancel()
        {
            cancellationSource.Cancel();
        }


    }
}
