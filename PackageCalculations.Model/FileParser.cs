﻿namespace PackageCalculations.Model
{
    using System;
    using System.Linq;
    using System.Reactive.Concurrency;
    using System.Reactive.Linq;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Reactive.Threading.Tasks;
    using System.Reactive;

    internal sealed class FileParser : IFileParser
    {
        private readonly string filepath;
        private readonly IFactory<string, IFileReader> readerFactory;
        private readonly IFactory<string, int, IFileWriter> writerFactory;
        private readonly ILineProcessor lineProcessor;
        private readonly IProgressReporter progressReporter;
        private readonly IScheduler taskScheduler;
        private IFileReader reader;
        private IFileWriter writer;
        private bool isParsing;

        public FileParser(string filepath, 
            IFactory<string, IFileReader> readerFactory,
            IFactory<string, int, IFileWriter> writerFactory,
            ILineProcessor lineProcessor,
            IProgressReporter progressReporter,
            IScheduler taskScheduler)
        {
            this.filepath = filepath;
            this.readerFactory = readerFactory;
            this.writerFactory = writerFactory;
            this.lineProcessor = lineProcessor;
            this.progressReporter = progressReporter;
            this.taskScheduler = taskScheduler;
        }

        public void Parse()
        {
            if(isParsing)
                throw new InvalidOperationException("Cannot start second parsing process");
            isParsing = true;
            taskScheduler.Schedule(ParseInternal);
        }

        public IObservable<IPreogressInfoModel> ProgressChanged
        {
            get { return progressReporter.ProgressChanged; }
        }

        private void ParseInternal()
        {
            reader = readerFactory.Create(filepath);

            var expressionsCount = GetExpressionsCount();
            progressReporter.SetTotal(expressionsCount);
            writer = writerFactory.Create(filepath, expressionsCount);


            Enumerable.Range(0, expressionsCount)
                .Select<int, IObservable<Unit>>(
                    i => 
                    {
                        var line = reader.ReadLine();
                        return lineProcessor.EnqueueLine(i, line, writer).ToObservable(); 
                    })
                .Merge()
                .Subscribe(
                _ => Report(),
                _ => OnCompleted(false),
                () => OnCompleted(!lineProcessor.IsCanceled)
                );

        }

        private void Report()
        {
            if (!lineProcessor.IsCanceled)
                progressReporter.Report();
        }

        private void OnCompleted(bool success)
        {
            if (success)
                writer.WriteAll();
            reader.Dispose();
            progressReporter.Complete();
        }

        private int GetExpressionsCount()
        {
            var s = reader.ReadLine();

            if (s == null)
                ThrowFormatException();

            // ReSharper disable once AssignNullToNotNullAttribute
            var expressionsCount = int.Parse(s);

            if (expressionsCount < 0)
                ThrowFormatException();

            return expressionsCount;
        }

        public void Cancel()
        {
             lineProcessor.Cancel();
        }

        private void ThrowFormatException()
        {
            throw new FormatException("File does not match the expected format");
        }

        public void Dispose()
        {
            Cancel();
            lineProcessor.Dispose();
        }
    }
}
