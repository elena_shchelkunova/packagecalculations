﻿namespace PackageCalculations.Model
{
    using PackageCalculations.Model.Operands;

    public interface IExpressionParser : ILongOperation
    {
        IOperand Parse();
    }
}
