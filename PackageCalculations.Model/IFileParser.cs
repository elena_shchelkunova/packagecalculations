﻿namespace PackageCalculations.Model
{
    using System;
    using System.Threading.Tasks;

    public interface IFileParser : ILongOperation, IDisposable
    {
        void Parse();
        IObservable<IPreogressInfoModel> ProgressChanged { get; }
    }
}
