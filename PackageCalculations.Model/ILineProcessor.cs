﻿namespace PackageCalculations.Model
{
    using System;
    using System.Threading.Tasks;
    using PackageCalculations.Model.ReaderWiters;

    internal interface ILineProcessor : ILongOperation, IDisposable
    {
        Task EnqueueLine(int position, string line, IFileWriter writer);
        bool IsCanceled { get; }
    }
}