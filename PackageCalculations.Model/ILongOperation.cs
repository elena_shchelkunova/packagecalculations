﻿namespace PackageCalculations.Model
{
    public interface ILongOperation
    {
        void Cancel();
    }
}