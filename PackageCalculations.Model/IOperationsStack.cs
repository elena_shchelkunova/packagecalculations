namespace PackageCalculations.Model
{
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Operations;

    internal interface IOperationsStack
    {
        IOperand CurNumber { get; }
        IOperand PrevNumber { get; }
        IOperation Operation { get; }

        void PushOperand(IOperand operand);

        void PushOperation(IOperation currentOperation);

        void ExecuteAll();
    }
}