﻿namespace PackageCalculations.Model
{
    using System;

    public interface IPreogressInfoModel
    {
        int Progress { get; }
        int ExpressionsDone { get; }
        TimeSpan TotalTime { get; }
        TimeSpan ExprctedRemainingTime { get; }
    }
}