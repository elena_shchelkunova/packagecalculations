﻿namespace PackageCalculations.Model
{
    using System;

    public interface IProgressReporter
    {
        void Report();
        void Complete();

        void SetTotal(int count);
        IObservable<IPreogressInfoModel> ProgressChanged { get; }
    }
}