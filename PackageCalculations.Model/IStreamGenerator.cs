namespace PackageCalculations.Model
{
    using PackageCalculations.Model.ReaderWiters;

    public interface IStreamGenerator
    {
        IReader GetBinaryReader(string source);
    }
}