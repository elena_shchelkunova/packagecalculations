﻿namespace PackageCalculations.Model
{
    using System;
    using System.Collections.Concurrent;
    using System.Reactive.Concurrency;
    using System.Reactive.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Practices.ObjectBuilder2;
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;
    using PackageCalculations.Model.Statistics;

    /// <summary>
    /// This class in configured to be run syncronously
    /// </summary>
    internal sealed class LineProcessor : ILineProcessor
    {
        private readonly IStatisticsReporter statisticsReporter;
        private readonly IFactory<string, IStatisticsReporter, IExpressionParser> expressionParserFactory;
        private readonly ConcurrentDictionary<Task, IExpressionParser> expressionParsers;
        private readonly CancellationTokenSource cts;
        private readonly IDisposable subscription;

        public LineProcessor(IStatisticsModel statisticsModel,
            IScheduler observeOnScheduler,
            IStatisticsReporter statisticsReporter,
            IFactory<string, IStatisticsReporter, IExpressionParser> expressionParserFactory)
        {
            this.statisticsReporter = statisticsReporter;
            this.expressionParserFactory = expressionParserFactory;

            cts = new CancellationTokenSource();
            expressionParsers = new ConcurrentDictionary<Task, IExpressionParser>();

            subscription =
               statisticsReporter.AsObservable()
               .ObserveOn(observeOnScheduler)
               .Subscribe(statisticsModel.OnNewData);
        }

        public Task EnqueueLine(int position, string line, IFileWriter writer)
        {
            var expressionParser = expressionParserFactory.Create(line, statisticsReporter);
            var task = Task<IOperand>.Factory.StartNew(expressionParser.Parse, cts.Token)
                .ContinueWith(res =>
                {
                    if (res.Status == TaskStatus.RanToCompletion)
                        writer.SaveValue(position, res.Result);
                }, cts.Token);

            expressionParsers.TryAdd(task, expressionParser);
            return task;
        }

        public bool IsCanceled
        {
            get { return cts.IsCancellationRequested; }
        }

        public void Cancel()
        {
            cts.Cancel();
            expressionParsers.Values.ForEach(x=>x.Cancel());
        }

        private void Dispose(bool disposing)
        {
            if (!disposing) 
                return;

            Cancel();
            subscription.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~LineProcessor()
        {
            Dispose(false);
        }

    }
}