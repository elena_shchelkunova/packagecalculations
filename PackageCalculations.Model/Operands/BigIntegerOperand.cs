﻿namespace PackageCalculations.Model.Operands
{
    using System;
    using System.Linq;
    using System.Numerics;
    using System.Text;
    using PackageCalculations.Model.ReaderWiters;

    internal sealed class BigIntegerOperand : IOperand
    {
        public BigIntegerOperand(IReader reader)
        {
            StringBuilder sb = new StringBuilder();
            while (Char.IsNumber((char)reader.PeekChar()))
            {
                sb.Append(reader.ReadChar());
            }

            Value = BigInteger.Parse(sb.ToString());
        }

        public BigInteger Value { get; set; }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}