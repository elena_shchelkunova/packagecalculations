﻿using System;
using System.Numerics;
namespace PackageCalculations.Model.Operands
{
    public interface IOperand
    {
        BigInteger Value { get; set; }
    }
}