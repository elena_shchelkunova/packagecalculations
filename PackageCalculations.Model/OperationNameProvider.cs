﻿namespace PackageCalculations.Model
{
    using System;
    using PackageCalculations.Model.Operations;
    using PackageCalculations.Model.Statistics;

    internal sealed class OperationNameProvider : IOperationNameProvider
    {
        private const string Default = "Other";

        public string DefaultValue
        {
            get { return Default; }
        }

        public string GetOperationName(Type T)
        {
            if (T == typeof( AddOperation))
                return "Add";

            if (T == typeof(DivideOperation))
                return "Divide";

            if (T == typeof(MultiplyOperation))
                return "Multiply";

            if (T == typeof(SubtractOperation))
                return "Subtract";

            if (T == typeof(UnaryMinusOperation))
                return "UnaryMinus";

            return Default;
        }

    }
}