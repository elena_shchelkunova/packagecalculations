﻿using System;

namespace PackageCalculations.Model.Operations
{
    using System.Threading;
    using PackageCalculations.Model.Operands;

    internal abstract class BaseOperation : IOperation
    {
        protected const int MAX_PRIORITY = 3;

        public abstract int Priority { get; }

        public virtual IOperand Execute(IOperand left, IOperand right)
        {
            Thread.Sleep(500);
            if (left == null || right == null)
                throw new FormatException("invalid syntax. Operand is not found");

            return null;
        }
    }
}
