﻿namespace PackageCalculations.Model.Operations
{
    using PackageCalculations.Model.Operands;

    public interface IOperation
    {
        int Priority { get; }
        IOperand Execute(IOperand left, IOperand right);
    }
}
