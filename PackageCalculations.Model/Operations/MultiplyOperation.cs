﻿namespace PackageCalculations.Model.Operations
{
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Statistics;

    internal sealed class MultiplyOperation : BaseOperation
    {
        private static int _priority = MAX_PRIORITY - 1;
        
        public override IOperand Execute(IOperand left, IOperand right)
        {
            base.Execute(left, right);
            left.Value *= right.Value;
            return left;
        }

        public override int Priority
        {
            get { return _priority; }
        }

        
    }
}
