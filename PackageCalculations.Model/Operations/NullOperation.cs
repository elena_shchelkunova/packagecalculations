﻿namespace PackageCalculations.Model.Operations
{
    using System;
    using PackageCalculations.Model.Operands;

    internal class NullOperation : IOperation
    {
        public int Priority
        {
            get { return -1; }
        }

        public IOperand Execute(IOperand left, IOperand right)
        {
            throw new NotSupportedException();
        }

        public int CompareTo(IOperation other)
        {
            throw new NotSupportedException();
        }

        public IOperand ExecuteAndCount(IOperand left, IOperand right)
        {
            throw new NotSupportedException();
        }
    }
}