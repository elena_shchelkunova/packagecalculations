﻿namespace PackageCalculations.Model.Operations
{
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Statistics;

    internal class OperationWithCounter : BaseOperation
    {
        private readonly IOperation operation;
        private readonly IStatisticsCounter statisticsCounter;
        public OperationWithCounter(IOperation operation, IStatisticsCounter statisticsCounter)
        {
            this.operation = operation;
            this.statisticsCounter = statisticsCounter;
        }

        public override int Priority { get { return operation.Priority; } }

        public override IOperand Execute(IOperand left, IOperand right)
        {
            statisticsCounter.Start();
            var result = operation.Execute(left, right);
            statisticsCounter.Stop();
            return result;
        }
    }
}