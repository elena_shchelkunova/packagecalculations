﻿namespace PackageCalculations.Model.Operations
{
    using PackageCalculations.Model.Operands;

    internal sealed class SubtractOperation : BaseOperation
    {
        private static int _priority = MAX_PRIORITY - 2;
       
        public override IOperand Execute(IOperand left, IOperand right)
        {
            base.Execute(left, right);
            left.Value -= right.Value;
            return left;
        }

        public override int Priority
        {
            get { return _priority; }
        }

        
    }
}
