﻿namespace PackageCalculations.Model.Operations
{
    using PackageCalculations.Model.Operands;

    internal sealed class UnaryMinusOperation : BaseOperation
    {
        private static int _priority = MAX_PRIORITY;

        public override IOperand Execute(IOperand left, IOperand right)
        {
            base.Execute(right, right);
            right.Value = -right.Value;
            return right;
        }

        public override int Priority
        {
            get { return _priority; }
        }
    }
}
