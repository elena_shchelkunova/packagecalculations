﻿using PackageCalculations.Model.Operations;

namespace PackageCalculations.Model
{
    using PackageCalculations.Model.Services;
    using PackageCalculations.Model.Statistics;

    internal sealed class OperationsFactory : IFactory<char, bool, IStatisticsReporter, IOperation>
    {
        public IOperation Create(char operation, bool isBegin, IStatisticsReporter statisticsReporter)
        {
            var result = CreateWithoutCounter(operation, isBegin);
            if(!(result is NullOperation))
                result = new OperationWithCounter(result, new StatisticsCounter(result.GetType(), statisticsReporter));
            return result;
        }

        public IOperation CreateWithoutCounter(char operation, bool isBegin)
        {
            if (isBegin && !operation.Equals('-'))
                return new NullOperation();

            switch (operation)
            {
                case '+':
                    return new AddOperation();

                case '-':
                    if (isBegin)
                        return new UnaryMinusOperation();

                    return new SubtractOperation();

                case '*':
                    return new MultiplyOperation();

                case '/':
                    return new DivideOperation();

                default:
                    return new NullOperation();
            }
        }

    }
}
