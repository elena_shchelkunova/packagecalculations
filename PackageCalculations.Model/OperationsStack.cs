﻿namespace PackageCalculations.Model
{
    using System;
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Operations;
    using PackageCalculations.Model.Wrappers;

    internal sealed class OperationsStack : IOperationsStack
    {
        private readonly IObjectStackWrapper objectStack;
        IOperand curNumber;
        IOperand prevNumber;
        IOperation operation;

        public OperationsStack(IObjectStackWrapper objectStack)
        {
            this.objectStack = objectStack;
        }

        public IOperand CurNumber
        {
            get { return curNumber; }
        }

        public IOperand PrevNumber
        {
            get { return prevNumber; }
        }

        public IOperation Operation
        {
            get { return operation; }
        }

        public void PushOperand(IOperand operand)
        {
            if (curNumber != null)
                throw new FormatException("Syntax error. Operator is missing");

            curNumber = operand;
        }

        public void PushOperation(IOperation currentOperation)
        {
            bool handled;

            if (Operation == null)
            {
                prevNumber = curNumber;
                handled = true;
            }
            else
                handled = TryExecuteOperationsInStack(currentOperation.Priority);

            if (!handled && prevNumber != null)
            {
                PushPreviuos();
            }

            operation = currentOperation;
            curNumber = null;
        }

        private void PushPreviuos()
        {
            objectStack.Push(prevNumber);
            objectStack.Push(operation);
            prevNumber = curNumber;
        }

        private bool TryExecuteOperationsInStack(int currentPriority)
        {
            var handled = false;
            while (operation.Priority >= currentPriority)
            {
                prevNumber = operation.Execute(prevNumber, curNumber);

                handled = true;
                if (objectStack.IsEmpty)
                    break;

                curNumber = prevNumber;
                if (((IOperation)objectStack.Peek()).Priority >= currentPriority)
                {
                    operation = (IOperation)objectStack.Pop();
                    prevNumber = (IOperand)objectStack.Pop();
                }

                else break;
            }
            return handled;
        }

        public void ExecuteAll()
        {
            prevNumber = operation != null ? operation.Execute(prevNumber, curNumber) : curNumber;

            while (!objectStack.IsEmpty)
            {
                curNumber = prevNumber;
                operation = (IOperation)objectStack.Pop();
                prevNumber = (IOperand)objectStack.Pop();
                prevNumber = operation.Execute(prevNumber, curNumber);
            }
        }
    }
}