﻿namespace PackageCalculations.Model
{
    using System;

    internal sealed class PreogressInfoModel : IPreogressInfoModel
    {
        private readonly int progress;
        private readonly int expressionsDone;
        private readonly TimeSpan totalTime;
        private readonly TimeSpan expectedRemainigTime;

        public PreogressInfoModel(int progress, int expressionsDone, TimeSpan totalTime, TimeSpan expectedRemainigTime)
        {
            this.progress = progress;
            this.expressionsDone = expressionsDone;
            this.totalTime = totalTime;
            this.expectedRemainigTime = expectedRemainigTime;
        }

        public int Progress
        {
            get { return progress; }
        }

        public int ExpressionsDone
        {
            get { return expressionsDone; }
        }

        public TimeSpan TotalTime
        {
            get { return totalTime; }
        }

        public TimeSpan ExprctedRemainingTime
        {
            get { return expectedRemainigTime; }
        }
    }
}