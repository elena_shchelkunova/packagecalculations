﻿namespace PackageCalculations.Model.Prism
{
    using System.Reactive.Subjects;
    using Microsoft.Practices.Unity;
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Operations;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;
    using PackageCalculations.Model.Statistics;
    using PackageCalculations.Model.Wrappers;

    public sealed class ModelExtension : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<IFileWriter, FileWriter>();
            Container.RegisterType<IFileReader, FileReader>();
            Container.RegisterType<IFileParser, FileParser>();
            Container.RegisterType<IFactory<char, bool, IStatisticsReporter, IOperation>, OperationsFactory>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IOperand, BigIntegerOperand>();
            Container.RegisterType<IExpressionParser, ExpressionParser>();
            Container.RegisterType<IStatisticsReporter, StatisticsReporter>();
            Container.RegisterType<IStatisticsCounter, StatisticsCounter>();
            Container.RegisterType<IOperationUnit, OperationUnit>();
            Container.RegisterType<IOperationsStack, OperationsStack>();
            Container.RegisterType<IStreamGenerator, StreamGenerator>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IReader, BinaryReaderWrapper>();
            Container.RegisterType<IOperationsStack, OperationsStack>();
            Container.RegisterType<IObjectStackWrapper, ObjectStackWrapper>();
            Container.RegisterType<IStatisticsModel, StatisticsModel>();
            Container.RegisterType<IProgressReporter, ProgressReporter>();
            Container.RegisterType<IStatisticsOperationModelInternal, StatisticsOperationModel>();
            Container.RegisterType<ITotalDurationCounter, TotalDurationCounter>();
            Container.RegisterType<IStatisticsOperationUnit, StatisticsOperationUnit>();
            Container.RegisterType<ILineProcessor, LineProcessor>();
            
            Container.RegisterType<IOperationNameProvider, OperationNameProvider>(new ContainerControlledLifetimeManager());
            
            Container.RegisterType(typeof(IFactory<>), typeof(Factory<>), new ContainerControlledLifetimeManager());
            Container.RegisterType(typeof(IFactory<,>), typeof(Factory<,>), new ContainerControlledLifetimeManager());
            Container.RegisterType(typeof(IFactory<,,>), typeof(Factory<,,>), new ContainerControlledLifetimeManager());
            Container.RegisterType(typeof(IFactory<,,,>), typeof(Factory<,,,>), new ContainerControlledLifetimeManager());

            Container.RegisterType(typeof (ISubject<>), typeof (Subject<>));

        }

       
    }
}

