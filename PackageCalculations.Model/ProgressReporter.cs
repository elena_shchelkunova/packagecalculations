﻿namespace PackageCalculations.Model
{
    using System;
    using System.Diagnostics;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;

    public class ProgressReporter : IProgressReporter
    {
        private readonly ISubject<IPreogressInfoModel> progress;
        private double step;
        private int calculatedExpressions;
        private readonly Stopwatch sw;
        private int totalExpressionsCount;

        public ProgressReporter(ISubject<IPreogressInfoModel> progress)
        {
            this.progress = progress;
            sw = new Stopwatch();
        }

        public IObservable<IPreogressInfoModel> ProgressChanged
        {
            get { return progress.AsObservable(); }
        }

        public void Report()
        {
            calculatedExpressions++;
            var newProgres = new PreogressInfoModel(
                progress: (int) (step*calculatedExpressions),
                expressionsDone: calculatedExpressions,
                totalTime: sw.Elapsed,
                expectedRemainigTime: new TimeSpan((long)((double)sw.ElapsedTicks / calculatedExpressions * totalExpressionsCount)));
            progress.OnNext(newProgres);
        }

        public void Complete()
        {
            sw.Stop();
            var finalProgres = new PreogressInfoModel(
                progress: (int)(step * calculatedExpressions),
                expressionsDone: calculatedExpressions,
                totalTime: sw.Elapsed,
                expectedRemainigTime: TimeSpan.Zero);

            progress.OnNext(finalProgres);
            progress.OnCompleted();
        }

        public void SetTotal(int count)
        {
            totalExpressionsCount = count;
            step = 100.0/count;
            sw.Restart();
        }
    }
}