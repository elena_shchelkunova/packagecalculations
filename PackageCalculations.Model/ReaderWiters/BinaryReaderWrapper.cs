﻿namespace PackageCalculations.Model.ReaderWiters
{
    using System.Diagnostics.CodeAnalysis;
    using System.IO;

    [ExcludeFromCodeCoverage]
    public class BinaryReaderWrapper : IReader
    {
        readonly BinaryReader reader;
        public BinaryReaderWrapper(Stream stream)
        {
            reader = new BinaryReader(stream);
        }

        public char ReadChar()
        {
            return reader.ReadChar();
        }

        public int PeekChar()
        {
            return reader.PeekChar();
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                // do stuff
            }
            reader.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~BinaryReaderWrapper()
        {
            Dispose(false);
        }
    }
}