﻿namespace PackageCalculations.Model.ReaderWiters
{
    using System.IO;
    using System.Threading.Tasks;

    internal sealed class FileReader : IFileReader
    {
        readonly StreamReader reader; 

        public FileReader(string filepath)
        {
            reader = new StreamReader(File.OpenRead(filepath));
        }

        public void Dispose()
        {
            if(reader != null)
                reader.Dispose();
        }

        public Task<string> ReadLineAsync()
        {
            return reader.ReadLineAsync();
        }


        public string ReadLine()
        {
            return reader.ReadLine();
        }
    }
}
