﻿namespace PackageCalculations.Model.ReaderWiters
{
    using System;
    using System.IO;
    using System.Threading;
    using PackageCalculations.Model.Operands;

    internal sealed class FileWriter : IFileWriter
    {
        private readonly string filepath;
        const string ErrorString = "ОШИБКА";
        readonly string[] resultStrings;

        public FileWriter(string filepath, int expressionsCount)
        {
            var dir = Path.GetDirectoryName(filepath);

            this.filepath = Path.Combine(dir ?? string.Empty,
                Path.GetFileNameWithoutExtension(filepath) + "_result" +
                Path.GetExtension(filepath));
            resultStrings = new string[expressionsCount];
        }

        public void SaveValue(int i, IOperand res)
        {
            var s = res != null ? res.ToString() : ErrorString;
            if(Interlocked.Exchange(ref resultStrings[i], s) != null)
                throw new Exception(string.Format("Error writing to file value {0} to index {1}. Expression already has value", s, i));
        }

        public void WriteAll()
        {
            using(var stream = File.OpenWrite(filepath))
            using (var streamWriter = new StreamWriter(stream))
            {
                foreach (var resultString in resultStrings)
                {
                    streamWriter.WriteLine(resultString);
                }
            }
        }
    }
}