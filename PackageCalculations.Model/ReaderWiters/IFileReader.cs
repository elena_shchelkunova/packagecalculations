﻿namespace PackageCalculations.Model.ReaderWiters
{
    using System;
    using System.Threading.Tasks;

    internal interface IFileReader : IDisposable
    {
        Task<string> ReadLineAsync();

        string ReadLine();
    }
}
