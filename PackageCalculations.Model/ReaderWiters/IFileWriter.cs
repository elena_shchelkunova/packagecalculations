﻿namespace PackageCalculations.Model.ReaderWiters
{
    using System;
    using PackageCalculations.Model.Operands;

    interface IFileWriter
    {
        void SaveValue(int i, IOperand value);
        void WriteAll();
    }
}
