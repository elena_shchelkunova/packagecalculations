﻿namespace PackageCalculations.Model.ReaderWiters
{
    using System;
    public interface IReader : IDisposable
    {
        char ReadChar();
        int PeekChar();
    }
}