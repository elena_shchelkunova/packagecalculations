﻿namespace PackageCalculations.Model.Services
{
    using System;
    using Microsoft.Practices.Unity;

    internal sealed class Factory<T> : IFactory<T>
    {
        private readonly IUnityContainer unityContainer;

        public Factory(IUnityContainer unityContainer)
        {
            if (unityContainer == null)
                throw new ArgumentNullException("unityContainer");

            this.unityContainer = unityContainer;
        }

        public T Create()
        {
            return unityContainer.Resolve<T>();
        }
    }
}