﻿namespace PackageCalculations.Model.Services
{
    using System;
    using Microsoft.Practices.Unity;

    internal sealed class Factory<TParam, TResult> : IFactory<TParam, TResult>
    {
        private readonly IUnityContainer unityContainer;

        public Factory(IUnityContainer unityContainer)
        {
            if (unityContainer == null) 
                throw new ArgumentNullException("unityContainer");

            this.unityContainer = unityContainer;
        }

        public TResult Create(TParam arg)
        {
            var dependency = new DependencyOverride<TParam>(arg);
            return unityContainer.Resolve<TResult>(dependency);
        }
    }
}