﻿namespace PackageCalculations.Model.Services
{
    using System;
    using Microsoft.Practices.Unity;

    internal sealed class Factory<TParam1, TParam2, TParam3, TResult> : IFactory<TParam1, TParam2, TParam3, TResult>
    {
        private readonly IUnityContainer unityContainer;

        public Factory(IUnityContainer unityContainer)
        {
            if (unityContainer == null) 
                throw new ArgumentNullException("unityContainer");
           
            this.unityContainer = unityContainer;
        }

        public TResult Create(TParam1 arg1, TParam2 arg2, TParam3 arg3)
        {
            var dependency1 = new DependencyOverride<TParam1>(arg1);
            var dependency2 = new DependencyOverride<TParam2>(arg2);
            var dependency3 = new DependencyOverride<TParam3>(arg3);
            return unityContainer.Resolve<TResult>(dependency1, dependency2, dependency3);
        }
    }
}