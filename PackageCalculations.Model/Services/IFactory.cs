﻿namespace PackageCalculations.Model.Services
{
    public interface IFactory<out TResult>
    {
        TResult Create();
    }
}
