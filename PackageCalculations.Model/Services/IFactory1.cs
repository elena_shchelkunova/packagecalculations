﻿namespace PackageCalculations.Model.Services
{
    public interface IFactory<in TParam, out TResult>
    {
        TResult Create(TParam param);
    }
}