﻿namespace PackageCalculations.Model.Services
{
    public interface IFactory<in TParam1, in TParam2, out TResult>
    {
        TResult Create(TParam1 param1, TParam2 param2);
    }
}