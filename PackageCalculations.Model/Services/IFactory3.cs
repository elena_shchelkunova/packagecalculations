﻿namespace PackageCalculations.Model.Services
{
    public interface IFactory<in TParam1, in TParam2, in TParam3, out TResult>
    {
        TResult Create(TParam1 param1, TParam2 param2, TParam3 param3);
    }
}