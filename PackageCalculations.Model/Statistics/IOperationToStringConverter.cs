namespace PackageCalculations.Model.Statistics
{
    using System;

    internal interface IOperationNameProvider
    {
        string GetOperationName(Type T);
        string DefaultValue { get; }
    }
}