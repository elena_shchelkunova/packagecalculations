﻿namespace PackageCalculations.Model.Statistics
{
    using System;

    public interface IOperationUnit
    {
        string Name { get; }
        TimeSpan Duration { get; }
    }
}