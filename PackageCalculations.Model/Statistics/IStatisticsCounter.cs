﻿namespace PackageCalculations.Model.Statistics
{
    using System;
    public interface IStatisticsCounter
    {
        void Start();
        void Stop();
    }
}
