namespace PackageCalculations.Model.Statistics
{
    using System.Collections.Generic;

    public interface IStatisticsModel
    {
        void OnNewData(IOperationUnit data);
        List<IStatisticsOperationModel> GetOperationsStatistics();
    }
}