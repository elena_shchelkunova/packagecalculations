﻿namespace PackageCalculations.Model.Statistics
{
    using System;

    public interface IStatisticsOperationModel
    {
        string OperationName { get; }
        IObservable<IStatisticsOperationUnit> Statistics { get; }

    }
}