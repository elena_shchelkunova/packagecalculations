﻿namespace PackageCalculations.Model.Statistics
{
    using System;

    internal interface IStatisticsOperationModelInternal : IStatisticsOperationModel
    {
        void Add(TimeSpan data);
    }
}