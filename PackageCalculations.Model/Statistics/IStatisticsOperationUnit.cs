﻿using System;
namespace PackageCalculations.Model.Statistics
{
    public interface IStatisticsOperationUnit
    {
        double Duration { get; }
        double AverageDuration { get; }
        Func<double> PercentOfAll { get; }
        int OperationsCount { get; } 
    }
}