﻿namespace PackageCalculations.Model.Statistics
{
    using System;

    internal interface IStatisticsReporter
    {
        void Report(Type type, TimeSpan timeSpan);

        IObservable<IOperationUnit> AsObservable();
    }
}