namespace PackageCalculations.Model.Statistics
{
    using System;

    internal interface ITotalDurationCounter
    {
        void Add(TimeSpan time);
        TimeSpan GetTotal();
    }
}