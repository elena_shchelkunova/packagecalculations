﻿namespace PackageCalculations.Model.Statistics
{
    using System;

    public sealed class OperationUnit : IOperationUnit
    {
        private readonly string name;
        private readonly TimeSpan duration;

        public OperationUnit(string name, TimeSpan duration)
        {
            this.name = name;
            this.duration = duration;
        }

        public string Name
        {
            get { return name; }
        }

        public TimeSpan Duration
        {
            get { return duration; }
        }
    }
}