﻿namespace PackageCalculations.Model.Statistics
{
    using System;
    using System.Diagnostics;

    internal sealed class StatisticsCounter : IStatisticsCounter
    {
        private readonly Type type;
        private readonly IStatisticsReporter statisticsReporter;
        readonly Stopwatch counter;

        public StatisticsCounter(Type type, IStatisticsReporter statisticsReporter)
        {
            this.type = type;
            this.statisticsReporter = statisticsReporter;
            counter = new Stopwatch();
        }

        public void Start()
        {
            counter.Start();
        }

        public void Stop()
        {
            counter.Stop();
            statisticsReporter.Report(type, counter.Elapsed);
        }
    }
}