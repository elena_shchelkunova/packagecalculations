﻿namespace PackageCalculations.Model.Statistics
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using PackageCalculations.Model.Operations;
    using PackageCalculations.Model.Services;

    internal sealed class StatisticsModel : IStatisticsModel
    {
        readonly Dictionary<string, IStatisticsOperationModelInternal> statistics;

        public StatisticsModel(IFactory<string, ITotalDurationCounter, IStatisticsOperationModelInternal> statisticsOperationModelFactory,
            IOperationNameProvider namesProvider,
            ITotalDurationCounter totalTimeCounter)
        {
            statistics = new Dictionary<string, IStatisticsOperationModelInternal>
            {
                {
                    namesProvider.GetOperationName(typeof (AddOperation)),
                    statisticsOperationModelFactory.Create("Сложение", totalTimeCounter)
                },
                {
                    namesProvider.GetOperationName(typeof (SubtractOperation)),
                    statisticsOperationModelFactory.Create("Вычитание", totalTimeCounter)
                },
                {
                    namesProvider.GetOperationName(typeof (MultiplyOperation)),
                    statisticsOperationModelFactory.Create("Умножение", totalTimeCounter)
                },
                {
                    namesProvider.GetOperationName(typeof (DivideOperation)),
                    statisticsOperationModelFactory.Create("Деление", totalTimeCounter)
                },
                {
                    namesProvider.GetOperationName(typeof (UnaryMinusOperation)),
                    statisticsOperationModelFactory.Create("Унарный минус", totalTimeCounter)
                }
            };
        }

        public void OnNewData(IOperationUnit data)
        {
            if (statistics.ContainsKey(data.Name))
                statistics[data.Name].Add(data.Duration);
        }

        public List<IStatisticsOperationModel> GetOperationsStatistics()
        {
            return statistics.Values.Cast<IStatisticsOperationModel>().ToList();
        }
    }
}