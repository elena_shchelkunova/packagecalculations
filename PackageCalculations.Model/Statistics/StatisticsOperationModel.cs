namespace PackageCalculations.Model.Statistics
{
    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;

    internal sealed class StatisticsOperationModel : IStatisticsOperationModelInternal
    {
        private readonly string operationName;
        private readonly ITotalDurationCounter totalTimeCounter;
        private int operationsCount;
        private TimeSpan duration;
        private readonly ISubject<IStatisticsOperationUnit> changedEvent;

        public StatisticsOperationModel(string operationName, 
            ITotalDurationCounter totalTimeCounter, 
            ISubject<IStatisticsOperationUnit> changedEvent)
        {
            this.operationName = operationName;
            this.totalTimeCounter = totalTimeCounter;
            this.changedEvent = changedEvent;
        }

        public string OperationName { get { return operationName; } }

        public int OperationsCount { get { return operationsCount; } }

        public double Duration { get { return duration.TotalSeconds; } }

        public double AverageDuration { get { return duration.TotalSeconds / operationsCount; } }

        public Func<double> PercentOfAll
        {
            get
            {
                return GetPercentOfAll;
            }
        }

        private double GetPercentOfAll()
        {
            return totalTimeCounter.GetTotal().Equals(TimeSpan.Zero)
                        ? 0.0
                        : duration.TotalSeconds / totalTimeCounter.GetTotal().TotalSeconds;
        }

        public void Add(TimeSpan delta)
        {
            duration += delta;
            totalTimeCounter.Add(delta);
            operationsCount++;
            var change = new StatisticsOperationUnit(operationsCount, 
                Duration / Environment.ProcessorCount, 
                AverageDuration / Environment.ProcessorCount, 
                PercentOfAll);
            changedEvent.OnNext(change);
        }


        public IObservable<IStatisticsOperationUnit> Statistics
        {
            get { return changedEvent.AsObservable(); }
        }
    }
}