﻿using System;
namespace PackageCalculations.Model.Statistics
{
    internal sealed class StatisticsOperationUnit : IStatisticsOperationUnit
    {
        private readonly double duration;
        private readonly double averageDuration;
        private readonly Func<double> percentOfAll;
        private readonly int operationsCount;

        public StatisticsOperationUnit(int operationsCount, double duration, double averageDuration, Func<double> percentOfAll)
        {
            this.duration = duration;
            this.averageDuration = averageDuration;
            this.percentOfAll = percentOfAll;
            this.operationsCount = operationsCount;
        }

        public double Duration
        {
            get { return duration; }
        }

        public double AverageDuration
        {
            get { return averageDuration; }
        }

        public Func<double> PercentOfAll
        {
            get { return percentOfAll; }
        }

        public int OperationsCount
        {
            get { return operationsCount; }
        }
    }
}