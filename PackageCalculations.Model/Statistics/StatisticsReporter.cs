﻿namespace PackageCalculations.Model.Statistics
{
    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using PackageCalculations.Model.Services;

    internal sealed class StatisticsReporter : IStatisticsReporter
    {
        readonly ISubject<IOperationUnit> statisticReporter;
        private readonly IFactory<string, TimeSpan, IOperationUnit> operationUnitFactory;
        private readonly IOperationNameProvider operationNameProvider;

        public StatisticsReporter(ISubject<IOperationUnit> statisticReporter, 
            IFactory<string, TimeSpan, IOperationUnit> operationUnitFactory,
            IOperationNameProvider operationNameProvider)
        {
            this.statisticReporter = statisticReporter;
            this.operationUnitFactory = operationUnitFactory;
            this.operationNameProvider = operationNameProvider;
        }

        public void Report(Type type, TimeSpan timeSpan)
        {
            var operationName = operationNameProvider.GetOperationName(type);
            if(operationName.Equals(operationNameProvider.DefaultValue))
                return;

            var data = operationUnitFactory.Create(operationName, timeSpan);
            Report(data);
        }

        public void Report(IOperationUnit data)
        {
            statisticReporter.OnNext(data);
        }

        public IObservable<IOperationUnit> AsObservable()
        {
            return statisticReporter.AsObservable();
        }
    }
}