﻿namespace PackageCalculations.Model.Statistics
{
    using System;

    internal sealed class TotalDurationCounter : ITotalDurationCounter
    {
        TimeSpan totalTime;
        public void Add(TimeSpan time)
        {
            totalTime += time;
        }

        public TimeSpan GetTotal()
        {
            return totalTime;
        }
    }
}