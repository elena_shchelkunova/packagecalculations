﻿namespace PackageCalculations.Model
{
    using System.IO;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;

    public class StreamGenerator : IStreamGenerator
    {
        private readonly IFactory<Stream, IReader> readerFactory;

        public StreamGenerator(IFactory<Stream, IReader> readerFactory)
        {
            this.readerFactory = readerFactory;
        }

        public IReader GetBinaryReader(string source)
        {
            var stream = GenerateStreamFromString(source);
            return readerFactory.Create(stream);
        }

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Write("\n");
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}