﻿namespace PackageCalculations.Model.Wrappers
{
    public interface IObjectStackWrapper
    {
        void Push(object obj);
        object Pop();

        object Peek();
        bool IsEmpty { get; }
    }
}