﻿namespace PackageCalculations.Model.Wrappers
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using PackageCalculations.Model.Operations;

    [ExcludeFromCodeCoverage]
    public class ObjectStackWrapper : IObjectStackWrapper
    {
        readonly Stack<object> stack = new Stack<object>();

        public void Push(object obj)
        {
            stack.Push(obj);
        }

        public object Pop()
        {
            return stack.Pop();
        }

        public object Peek()
        {
            return stack.Peek();
        }

        public bool IsEmpty
        {
            get { return !stack.Any(); }
        }
    }
}