﻿namespace PackageCalculations.Tests
{
    using System;
    using Moq;
    using NUnit.Framework;
    using PackageCalculations.Model;
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Operations;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;
    using PackageCalculations.Model.Statistics;
    using Shouldly;

    [TestFixture]
    public class ExpressionParserFixture
    {
        string expression;
        Mock<IFactory<IReader, IOperand>> operandFactory;
        Mock<IFactory<IOperationsStack>> operationsStackFactory;
        Mock<IFactory<char, bool, IStatisticsReporter, IOperation>> operationFactory;
        Mock<IStreamGenerator> streamGenerator;
        Mock<IOperationsStack> operaionsStack;
        Mock<IOperation> operation;
        Mock<IOperation> nullOperation;
        Mock<IOperand> operand;
        Mock<IReader> reader;
        Mock<IStatisticsReporter> statisticsReporter;

        [SetUp]
        public void Setup()
        {
            expression = "1";
            operandFactory = new Mock<IFactory<IReader, IOperand>>();
            operand = new Mock<IOperand>();
            operandFactory.Setup(x => x.Create(It.IsAny<IReader>())).Returns(operand.Object);
            operationsStackFactory = new Mock<IFactory<IOperationsStack>>();
            operaionsStack = new Mock<IOperationsStack>();
            operationsStackFactory.Setup(x => x.Create()).Returns(operaionsStack.Object);
            operationFactory = new Mock<IFactory<char, bool,IStatisticsReporter, IOperation>>();
            operation = new Mock<IOperation>();
            operation.SetupGet(x => x.Priority).Returns(1);
            nullOperation = new Mock<IOperation>();
            nullOperation.SetupGet(x => x.Priority).Returns(0);
            operationFactory.Setup(x => x.Create(It.IsIn('+', '-', '*', '/'), It.IsAny<bool>(), It.IsAny<IStatisticsReporter>()))
                .Returns(operation.Object);
            operationFactory.Setup(x => x.Create(It.IsNotIn('+', '-', '*', '/'), It.IsAny<bool>(), It.IsAny<IStatisticsReporter>()))
                .Returns(nullOperation.Object);
            streamGenerator = new Mock<IStreamGenerator>();
            reader = new Mock<IReader>();
            streamGenerator.Setup(x => x.GetBinaryReader(It.IsAny<string>()))
                .Returns(reader.Object);

            statisticsReporter = new Mock<IStatisticsReporter>();
        }

        [Test]
        public void ShouldInitializeProperly()
        {
            // When
            var instance = CreateInstance();
            // Then
            instance.ShouldNotBeNull();
        }

        [Test]
        public void ShouldGenerateBinaryReaderFromString()
        {
            // Given
            string passedString = string.Empty;
            streamGenerator.Setup(x => x.GetBinaryReader(It.IsAny<string>()))
                .Returns(Mock.Of<IReader>())
                .Callback<string>(s => passedString = s);
            operation.SetupGet(x => x.Priority).Returns(-1);
            var instance = CreateInstance();
            // When
            instance.Parse();
            // Then
            streamGenerator.Verify(x => x.GetBinaryReader(It.IsAny<string>()), Times.Once);
            passedString.ShouldBe(expression);
        }

        [Test]
        [ExpectedException(typeof(OperationCanceledException))]
        public void ShouldThrowIfCancelRequwsted()
        {
            //Given
            var instance = CreateInstance();

            // When
            instance.Cancel();
            instance.Parse();
        }

        [Test]
        public void ShouldCreateAndPushOperandOnNumber([Values(0,1,2,3,4,5,6,7,8,9)] int c)
        {
            //Given
            reader.SetupSequence(x => x.PeekChar()).Returns(char.Parse(c.ToString())).Returns('\n');
            IOperand acual = null;
            operaionsStack.Setup(x => x.PushOperand(It.IsAny<IOperand>())).Callback<IOperand>(op => acual = op);
            var instance = CreateInstance();

            // When
            instance.Parse();

            //Then
            operandFactory.Verify(x => x.Create(It.IsAny<IReader>()), Times.Once);
            operaionsStack.Verify(x => x.PushOperand(It.IsAny<IOperand>()), Times.Once);
            acual.ShouldBe(operand.Object);
        }

        [Test]
        public void ShouldCreateAndPushOperatorOnOperation([Values("+", "-", "*", "/")] string s)
        {
            //Given
            reader.SetupSequence(x => x.PeekChar()).Returns('0').Returns(char.Parse(s)).Returns('5').Returns('\n');
            IOperation acual = null;
            char actualChar = char.MinValue;
            operation.SetupSequence(x => x.Priority).Returns(1).Returns(0);
            operationFactory.Setup(x => x.Create(It.IsIn('+', '-', '*', '/'), It.IsAny<bool>(), It.IsAny<IStatisticsReporter>()))
                .Returns(operation.Object)
                .Callback<char, bool, IStatisticsReporter>((c, b, _) =>
            {
                actualChar = c;
            });
            
            operaionsStack.Setup(x => x.PushOperation(It.IsAny<IOperation>())).Callback<IOperation>(op => acual = op);

            var instance = CreateInstance();

            // When
            instance.Parse();

            //Then
            operationFactory.Verify(x => x.Create(It.IsAny<char>(), It.IsAny<bool>(), It.IsAny<IStatisticsReporter>()), Times.AtLeastOnce);
            operaionsStack.Verify(x => x.PushOperation(It.IsAny<IOperation>()), Times.Once);
            actualChar.ShouldBe(char.Parse(s));
            acual.ShouldBe(operation.Object);
        }

        [Test]
        public void ShouldIgnoreSpace()
        {
            // Given
            reader.SetupSequence(x => x.PeekChar()).Returns('0').Returns(' ').Returns('\n');
            operand.SetupGet(x => x.Value).Returns(0);
            operaionsStack.SetupGet(x => x.PrevNumber).Returns(operand.Object);
            var instance = CreateInstance();
            // When
            var result = instance.Parse();
            // Then
            result.Value.ShouldBe(0);
        }

        [Test]
        public void ShouldCreateStackOnOpeningBracket()
        {
            // Given
            reader.SetupSequence(x => x.PeekChar()).Returns('-').Returns('(').Returns('-').Returns('1').Returns(')').Returns('\n');
            var instance = CreateInstance();

            // When
            instance.Parse();

            // Then
            operationsStackFactory.Verify(x=>x.Create(), Times.Exactly(2));
        }

        [Test]
        public void ShouldCalculateStackOnEnd()
        {
            // Given
            reader.SetupSequence(x => x.PeekChar()).Returns('-').Returns('1').Returns('+').Returns('2').Returns('\n');
            var instance = CreateInstance();

            // When
            instance.Parse();

            // Then
            operaionsStack.Verify(x=>x.ExecuteAll(), Times.Once);
        }


        [Test]
        public void ShouldReturnNullOnEmptyLine()
        {
            // Given
            reader.Setup(x => x.PeekChar()).Returns('\n');
            var instance = CreateInstance();

            // When
            var result = instance.Parse();

            // Then
            result.ShouldBe(null);
        }

        [Test]
        public void ShouldReturnNullOnDivideByZero()
        {
            // Given
            reader.SetupSequence(x => x.PeekChar()).Returns('-').Returns('5').Returns('/').Returns('0').Returns('\n');
            operation.Setup(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>())).Throws<DivideByZeroException>();
            var instance = CreateInstance();
            
            // When
            var result = instance.Parse();

            // Then
            result.ShouldBeNull();
        }

        [Test]
        public void ShouldReturnNullOnOverflow()
        {
            // Given
            reader.SetupSequence(x => x.PeekChar()).Returns('9').Returns('9').Returns('9').Returns('9').Returns('\n');
            operation.Setup(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>())).Throws<OverflowException>();
            var instance = CreateInstance();

            // When
            var result = instance.Parse();

            // Then
            result.ShouldBeNull();
        }

        [Test]
        public void ShouldReturnNullOnFormatException()
        {
            // Given
            reader.SetupSequence(x => x.PeekChar()).Returns('-').Returns('+').Returns('/').Returns('0').Returns('\n');
            operation.Setup(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>())).Throws<FormatException>();
            var instance = CreateInstance();

            // When
            var result = instance.Parse();

            // Then
            result.ShouldBeNull();
        }

        [Test]
        public void ShouldPassStatisticsReporterToOperations()
        {
            // Given
            reader.SetupSequence(x => x.PeekChar()).Returns('-').Returns('\n');
            IStatisticsReporter stReporter = null;
            operationFactory.Setup(x => x.Create(It.IsAny<char>(), It.IsAny<bool>(), It.IsAny<IStatisticsReporter>()))
                .Returns(Mock.Of<IOperation>())
                .Callback<char, bool, IStatisticsReporter>((_, __, r) => stReporter = r);
            var instance = CreateInstance();
            // When
            instance.Parse();
            // Then
            stReporter.ShouldBe(statisticsReporter.Object);

        }

        private ExpressionParser CreateInstance()
        {
            return new ExpressionParser(expression,
                operandFactory.Object,
                operationsStackFactory.Object,
                operationFactory.Object,
                streamGenerator.Object,
                statisticsReporter.Object);
        }
    }
}