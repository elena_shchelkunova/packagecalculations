﻿namespace PackageCalculations.Tests
{
    using System;
    using System.IO;
    using System.Numerics;
    using System.Reactive.Subjects;
    using Microsoft.Practices.Unity;
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Services;
    using NUnit.Framework;
    using PackageCalculations.Model;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Statistics;
    using PackageCalculations.Model.Wrappers;
    using Shouldly;

    [TestFixture]
    public class ExpressionParserIbtegrationTest
    {
        UnityContainer testContainer;
        [SetUp]
        public void Setup()
        {
            testContainer = new UnityContainer();
            testContainer.RegisterType<IOperand, BigIntegerOperand>();
            testContainer.RegisterType<IStatisticsCounter, StatisticsCounter>();
            testContainer.RegisterType<IOperationsStack, OperationsStack>();
            testContainer.RegisterType<IReader, BinaryReaderWrapper>();
            testContainer.RegisterType<IOperationsStack, OperationsStack>();
            testContainer.RegisterType<IReader, BinaryReaderWrapper>();
            testContainer.RegisterType<IObjectStackWrapper, ObjectStackWrapper>();
            testContainer.RegisterType<IOperationUnit, OperationUnit>();
            testContainer.RegisterType<IStreamGenerator, StreamGenerator>(new ContainerControlledLifetimeManager());
            testContainer.RegisterType(typeof(IFactory<>), typeof(Factory<>), new ContainerControlledLifetimeManager());
            
        }

        [TestCase("1+ 2-3 +4", 4)]
        [TestCase("- 1*9", -9)]
        [TestCase("-( -2)-(-4)", 6)]
        [TestCase("1+ 2*19- 3", 36)]
        [TestCase("- 1/2-6+8*5/2", 14)]
        [TestCase("5 88", null)]
        [TestCase("5/*6", null)]
        [TestCase("((", null)]
        [TestCase("32", 32)]
        [TestCase("859-635/2", 542)]
        [TestCase("-9*+6", null)]
        [TestCase("/9*+6", null)]
        public void ShouldParse(string input, int? expected)
        {
            // Given
            var parser = CreateInstance(input + '\n');

            // When
            var actual = parser.Parse();

            // Then
            if(expected == null)
                actual.ShouldBeNull();
            else
            actual.Value.ShouldBe(expected.Value);
        }

        private ExpressionParser CreateInstance(string s)
        {
            return new ExpressionParser(s, 
                new Factory<IReader, IOperand>(testContainer), 
                 new Factory<IOperationsStack>(testContainer), 
                 new OperationsFactory(),
                 new StreamGenerator(new Factory<Stream, IReader>(testContainer)),
                 new StatisticsReporter(new Subject<IOperationUnit>(), new Factory<string, TimeSpan, IOperationUnit>(testContainer), new OperationNameProvider()));
        }
    }
}
