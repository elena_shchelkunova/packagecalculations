﻿namespace PackageCalculations.Tests
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Reactive.Concurrency;
    using System.Threading.Tasks;
    using Microsoft.Practices.ObjectBuilder2;
    using Moq;
    using NUnit.Framework;
    using PackageCalculations.Model;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;
    using Shouldly;

    [TestFixture]
    public class FileParserFixture
    {
        private string filepath;
        private Mock<IFactory<string, IFileReader>> readerFactory;
        private Mock<IFactory<string, int, IFileWriter>> writerFactory;

        private Mock<IProgressReporter> progress;
        private Mock<IScheduler> taskScheduler;
        private Mock<IFileReader> reader;
        private Mock<IFileWriter> writer;

        private Mock<ILineProcessor> lineProcessor;

        [SetUp]
        public void Setup()
        {

            filepath = It.IsAny<string>();
            progress = new Mock<IProgressReporter>();

            taskScheduler = new Mock<IScheduler>();
            taskScheduler.Setup(x => x.Schedule(It.IsAny<Action>(), It.IsAny<Func<IScheduler, Action, IDisposable>>()))
                .Returns(Mock.Of<IDisposable>())
                .Callback<Action, Func<IScheduler, Action, IDisposable>>((a, func) => func(Mock.Of<IScheduler>(), a));
            reader = new Mock<IFileReader>();

            readerFactory = new Mock<IFactory<string, IFileReader>>();
            readerFactory.Setup(x => x.Create(It.IsAny<string>()))
                .Returns(Mock.Of<IFileReader>());

            writer = new Mock<IFileWriter>();

            writerFactory = new Mock<IFactory<string, int, IFileWriter>>();
            writerFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<int>())).Returns(Mock.Of<IFileWriter>());

            lineProcessor = new Mock<ILineProcessor>();
        }

        [Test]
        public void ShouldRunOnSchedulerThread()
        {
            // Given
            SetupSequenceFor(0);
            AssignReaderToFactory();

            var instance = CreateInstance();
            // When
            instance.Parse();

            // Then
            taskScheduler.Verify(x => x.Schedule(It.IsAny<Action>(), It.IsAny<Func<IScheduler, Action, IDisposable>>()), Times.Once);
        }

        [Test]
        public void ShouldReportCompleted()
        {
            // Given
            SetupSequenceFor(0);
            AssignReaderToFactory();
            var instance = CreateInstance();

            // When
            instance.Parse();
            
            // Then
            progress.Verify(x=>x.Complete(), Times.Once);
        }

        [Test]
        public  void ShouldCreateReader()
        {
            // Given
            reader.Setup(x => x.ReadLine()).Returns("0");
            AssignReaderToFactory();

            var instance = CreateInstance();
            // When
            instance.Parse();
            
            // Then
            readerFactory.Verify(x=>x.Create(It.IsAny<string>()), Times.Once);
            reader.Verify(x=>x.ReadLine(), Times.Once);
        }

        [Test]
        public void ShouldCreateWriter([Random(1, 150, 5)] int expressionsCount)
        {
            // Given
            SetupSequenceFor(expressionsCount);
           
            AssignReaderToFactory();
            
            int actualCount = -1;
           
            writerFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<int>())).Returns(writer.Object)
                .Callback<string, int>((_, n) => actualCount = n);

            var instance = CreateInstance();
            // When
            instance.Parse();

            // Then
            writerFactory.Verify(x => x.Create(It.IsAny<string>(), It.IsAny<int>()), Times.Once);
            actualCount.ShouldBe(expressionsCount);
        }

        [Test]
        public void ShouldReadAllLines([Random(1, 150, 5)] int expressionsCount)
        {
            // Given
            SetupSequenceFor(expressionsCount);
            lineProcessor.Setup(x => x.IsCanceled).Returns(false);
            AssignReaderToFactory();
            AssignWriterToFactory();

            var instance = CreateInstance();

            // When
            instance.Parse();

            // Then
            reader.Verify(x => x.ReadLine(), Times.Exactly(expressionsCount + 1));
        }

        [Test]
        public void ShouldCreateWriteResults([Random(1, 150, 5)] int expressionsCount)
        {
            // Given
            var results = new ConcurrentDictionary<int, string>();
            var writers = new ConcurrentBag<IFileWriter>();
            SetupSequenceFor(expressionsCount);
            lineProcessor.Setup(x => x.EnqueueLine(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<IFileWriter>()))
                .Returns( Task.Factory.StartNew(() => { }))
                .Callback<int, string, IFileWriter>((i, s, wr) =>
            {
                writers.Add(wr);
                results.TryAdd(i, s);
            });
            lineProcessor.Setup(x => x.IsCanceled).Returns(false);
            AssignReaderToFactory();
            AssignWriterToFactory();

            var instance = CreateInstance();

            // When
            instance.Parse();

            // Then
            lineProcessor.Verify(x=>x.EnqueueLine(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<IFileWriter>()), Times.Exactly(expressionsCount));
            results.Keys.Count.ShouldBe(expressionsCount);
            results.Keys.ShouldAllBe(x=>x<expressionsCount);
            results.Keys.ShouldBeUnique();
            results.Keys.ShouldAllBe(x=>x < expressionsCount);
            results.Values.ForEach(x => x.ShouldBe(expressionsCount.ToString()));

            writers.ShouldAllBe(x=>x.Equals(writer.Object));

            writer.Verify(x => x.WriteAll(), Times.Once);
        }

        [Test]
        public void ShouldReportProgressOnExpressionDone([Random(1, 150, 5)] int expressionsCount)
        {
            // Given
            SetupSequenceFor(expressionsCount);
            AssignReaderToFactory();
            int total = 0;
            progress.Setup(x => x.SetTotal(It.IsAny<int>())).Callback<int>(i => total = i);
            lineProcessor.Setup(x => x.IsCanceled).Returns(false);
            var instance = CreateInstance();
            // When
            instance.Parse();

            // Then
            progress.Verify(x=>x.SetTotal(It.IsAny<int>()), Times.Once);
            total.ShouldBe(expressionsCount);
            progress.Verify(x=>x.Complete(), Times.Once);
            progress.Verify(x => x.Report(), Times.Exactly(expressionsCount));
        }

        [Test]
        [ExpectedException(typeof(FormatException))]
        public void ShouldRaiseFormatExceptionOnWrongInput()
        {
            // Given
            reader.Setup(x => x.ReadLine()).Returns("aaa");
            AssignReaderToFactory();

            var instance = CreateInstance();
            // When
            instance.Parse();
        }

        [Test]
        public void ShouldReportCompletedAfterError()
        {
            // Given
            reader.Setup(x => x.ReadLine()).Returns("0");
            AssignReaderToFactory();
            var instance = CreateInstance();
            // When
            instance.Parse();

            // Then
            progress.Verify(x=>x.Complete(), Times.Once);
        }

        private void AssignReaderToFactory()
        {
            readerFactory.Setup(x => x.Create(It.IsAny<string>())).Returns(reader.Object);
        }

        private void AssignWriterToFactory()
        {
            writerFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<int>())).Returns(writer.Object);
        }

        private void SetupSequenceFor(int expressionsCount)
        {
            reader.Setup(x => x.ReadLine()).Returns(expressionsCount.ToString());
        }

        private FileParser CreateInstance()
        {
            return new FileParser(filepath,
                readerFactory.Object,
                writerFactory.Object,
                lineProcessor.Object,          
                progress.Object,
                taskScheduler.Object);
        }
    }
}