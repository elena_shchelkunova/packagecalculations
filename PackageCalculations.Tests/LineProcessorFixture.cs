﻿namespace PackageCalculations.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Reactive.Concurrency;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using System.Threading;
    using Moq;
    using NUnit.Framework;
    using PackageCalculations.Model;
    using PackageCalculations.Model.Services;
    using PackageCalculations.Model.Statistics;
    using Shouldly;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Operands;

    [TestFixture]
    public class LineProcessorFixture
    {
        private Mock<IStatisticsModel> statisticsModel;
        private Mock<IScheduler> observeOnScheduler;
        private Mock<IStatisticsReporter> statisticsReporter;
        private Mock<IFactory<string, IStatisticsReporter, IExpressionParser>> expressionParserFactory;
        Mock<IObservable<IOperationUnit>> observable;

        [SetUp] 
        public void SetUp()
        {
            statisticsModel = new Mock<IStatisticsModel>();
            observeOnScheduler = new Mock<IScheduler>();
            observeOnScheduler.Setup(x => x.Schedule(It.IsAny<Action>(), It.IsAny <Func<IScheduler, Action, IDisposable>>()))
                .Returns(Mock.Of<IDisposable>())
                .Callback<Action, Func<IScheduler, Action, IDisposable>>((a, func) => func(Mock.Of<IScheduler>(), a));
            
            statisticsReporter = new Mock<IStatisticsReporter>();
            observable = new Mock<IObservable<IOperationUnit>>();
            statisticsReporter.Setup(x => x.AsObservable()).Returns(observable.Object);

            expressionParserFactory = new Mock<IFactory<string, IStatisticsReporter, IExpressionParser>>();
        }

        [Test]
        public void ShouldSubscribetatisticsReporter()
        {

            // When
            CreateInstance();

            // Then
            statisticsReporter.Verify(x => x.AsObservable(), Times.Once);
            observable.Verify(x => x.Subscribe(It.IsAny<IObserver<IOperationUnit>>()), Times.Once);
        }

        [Test]
        public void ShouldCreateParsersOnEnqueueLine()
        {
            // Given
            var expressionParser = SetupExpressionParser();

            var instance = CreateInstance();

            // When
            instance.EnqueueLine(0, It.IsAny<string>(), Mock.Of<IFileWriter>());

            // Then
            expressionParserFactory.Verify(x => x.Create(It.IsAny<string>(), It.IsAny<IStatisticsReporter>()), Times.Once);
        }

        private Mock<IExpressionParser> SetupExpressionParser()
        {
            var parser = new Mock<IExpressionParser>();
            parser.Setup(x => x.Parse()).Returns(Mock.Of<IOperand>());
            expressionParserFactory.Setup(x => x.Create(It.IsAny<string>(), It.IsAny<IStatisticsReporter>()))
                .Returns(parser.Object);
            return parser;
        }

        private LineProcessor CreateInstance()
        {
            return new LineProcessor(statisticsModel.Object,
                observeOnScheduler.Object, 
                statisticsReporter.Object,
                expressionParserFactory.Object);
        }
    }
}
