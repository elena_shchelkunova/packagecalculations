﻿namespace PackageCalculations.Tests
{
    using System;
    using Moq;
    using NUnit.Framework;
    using PackageCalculations.Model;
    using PackageCalculations.Model.Operands;
    using PackageCalculations.Model.Operations;
    using PackageCalculations.Model.Wrappers;
    using Shouldly;
    using System.Collections.Generic;

    [TestFixture]
    public class OperationsStackFixture
    {
        Mock<IObjectStackWrapper> objectStack;

        [SetUp]
        public void Setup()
        {
            objectStack = new Mock<IObjectStackWrapper>();
        }
        [Test]
        public void ShouldPushOperand()
        {
            // Given
            var instance = CreateInstance();
            Mock<IOperand> operand = new Mock<IOperand>();

            // When
            instance.PushOperand(operand.Object);

            // Then
            instance.CurNumber.ShouldBe(operand.Object);
        }

        [Test]
        [ExpectedException(typeof(FormatException))]
        public void ShouldThrowOnSecondPushOperand()
        {
            // Given
            var instance = CreateInstance();

            // When
            instance.PushOperand(Mock.Of<IOperand>());
            instance.PushOperand(Mock.Of<IOperand>());

        }

        [Test]
        public void ShouldPushOperation()
        {
            // Given
            var instance = CreateInstance();
            Mock<IOperation> operation = new Mock<IOperation>();

            // When
            instance.PushOperation(operation.Object);

            // Then
            instance.Operation.ShouldBe(operation.Object);
        }

        [Test]
        public void ShouldPushOperationAndNumber()
        {
            // Given
            var instance = CreateInstance();
            var operand = new Mock<IOperand>();
            instance.PushOperand(operand.Object);
            var operation = new Mock<IOperation>();

            // When
            instance.PushOperation(operation.Object);
            
            // Then
            instance.CurNumber.ShouldBeNull();
            instance.PrevNumber.ShouldBe(operand.Object);
            instance.Operation.ShouldBe(operation.Object);
        }

        [Test]
        public void ShouldExecuteHighPriorityOperations()
        {
            // Given
            var instance = CreateInstance();
            var operation = new Mock<IOperation>();
            operation.SetupGet(x => x.Priority).Returns(3);
            var operation2 = new Mock<IOperation>();
            operation2.SetupGet(x => x.Priority).Returns(1);
            var operand = new Mock<IOperand>();
            objectStack.SetupGet(x => x.IsEmpty).Returns(true);
            IOperand actual1 = null, actual2 = null;
            Mock<IOperand> newOperand = new Mock<IOperand>();
            operation.Setup(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>())).Returns(newOperand.Object)
                .Callback<IOperand, IOperand>((o1, o2) =>
                {
                    actual1 = o1;
                    actual2 = o2;
                });
            instance.PushOperation(operation.Object);
            instance.PushOperand(operand.Object);
            
            // When
            instance.PushOperation(operation2.Object);

            // Then
            operation.Verify(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>()), Times.Once);
            operation2.Verify(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>()), Times.Never);
            objectStack.Verify(x=>x.Push(It.IsAny<object>()), Times.Never);

            actual1.ShouldBeNull();
            actual2.ShouldBe(operand.Object);
            instance.Operation.ShouldBe(operation2.Object);
            instance.PrevNumber.ShouldBe(newOperand.Object);
        }

        [Test]
        public void ShouldPushToStackIfLowPriorityOperations()
        {
            // Given
            var instance = CreateInstance();
            var operation = new Mock<IOperation>();
            operation.SetupGet(x => x.Priority).Returns(1);
            var operation2 = new Mock<IOperation>();
            operation2.SetupGet(x => x.Priority).Returns(2);
            var operand = new Mock<IOperand>();
            var operand2 = new Mock<IOperand>();
            List<object> objects = new List<object>();
            objectStack.Setup(x => x.Push(It.IsAny<object>())).Callback<object>(x => objects.Add(x));
            instance.PushOperand(operand.Object);
            instance.PushOperation(operation.Object);
            instance.PushOperand(operand2.Object);

            // When
            instance.PushOperation(operation2.Object);

            // Then
            operation.Verify(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>()), Times.Never);
            operation2.Verify(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>()), Times.Never);
            objectStack.Verify(x => x.Push(It.IsAny<object>()), Times.Exactly(2));
            objects.Count.ShouldBe(2);
            objects[0].ShouldBe(operand.Object);
            objects[1].ShouldBe(operation.Object);
            
            instance.Operation.ShouldBe(operation2.Object);
            instance.PrevNumber.ShouldBe(operand2.Object);
            instance.CurNumber.ShouldBeNull();
        }

        [Test]
        public void ShouldExecuteLastExecuteAll()
        {
            // Given
            var instance = CreateInstance();
            var operation = new Mock<IOperation>();
            Mock<IOperand> newOperand = new Mock<IOperand>();
            IOperand actual1 = null, actual2 = null;
            operation.Setup(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>()))
                .Returns(newOperand.Object)
                .Callback<IOperand, IOperand>((op1, op2) => 
                {
                    actual1 = op1;
                    actual2 = op2;
                });

            var operand = new Mock<IOperand>();
            var operand2 = new Mock<IOperand>();

            objectStack.SetupGet(x => x.IsEmpty).Returns(true);
            instance.PushOperand(operand.Object);
            instance.PushOperation(operation.Object);
            instance.PushOperand(operand2.Object);

            // When
            instance.ExecuteAll();

            // Then
            operation.Verify(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>()), Times.Once);
            actual1.ShouldBe(operand.Object);
            actual2.ShouldBe(operand2.Object);
            instance.PrevNumber.ShouldBe(newOperand.Object);

            objectStack.Verify(x=>x.Pop(), Times.Never);
        }

        [Test]
        public void ShouldEmptyStackOnExecuteAll()
        {
            // Given
            var instance = CreateInstance();
            var operation = new Mock<IOperation>();
            
            var operand = new Mock<IOperand>();
            objectStack.SetupSequence(x => x.Pop()).Returns(operation.Object).Returns(operand.Object).Returns(operation.Object).Returns(operand.Object);
            objectStack.SetupSequence(x => x.IsEmpty).Returns(false).Returns(false).Returns(true).Returns(true);

            // When
            instance.ExecuteAll();

            // Then
            objectStack.Verify(x => x.Pop(), Times.Exactly(4));
            operation.Verify(x => x.Execute(It.IsAny<IOperand>(), It.IsAny<IOperand>()), Times.Exactly(2));
        }

        private OperationsStack CreateInstance()
        {
            return new OperationsStack(objectStack.Object);
        }
 
    }
}