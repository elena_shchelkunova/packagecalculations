﻿namespace PackageCalculations.Tests
{
    using System.IO;
    using Moq;
    using NUnit.Framework;
    using PackageCalculations.Model;
    using PackageCalculations.Model.ReaderWiters;
    using PackageCalculations.Model.Services;
    using Shouldly;

    [TestFixture]
    public class StreamGeneratorFixture
    {
        Mock<IFactory<Stream, IReader>> readerFactory;
        Mock<IReader> reader;
        
        [SetUp]
        public void Setup()
        {
            readerFactory = new Mock<IFactory<Stream, IReader>>();
            reader = new Mock<IReader>();
            
        }

        [Test]
        public void ShouldCreateStreamFromString()
        {
            // Given
            string original = "1+6*9-87-8";

            readerFactory.Setup(x => x.Create(It.IsAny<Stream>())).Returns(reader.Object);
            var instance = CreateInstance();
            // When
            var result = instance.GetBinaryReader(original);
            // Then
            result.ShouldNotBeNull();
            result.ShouldBe(reader.Object);
        }

        [Test]
        public void ShouldReturnProperEndedStream()
        {
            // Given
            string original = "1+6*9-87-8";
            Stream actual = null;

            readerFactory.Setup(x => x.Create(It.IsAny<Stream>())).Returns(reader.Object).Callback<Stream>(s => actual = s);
            var instance = CreateInstance();
            // When
            instance.GetBinaryReader(original);
            // Then
            using (var binaryReader = new BinaryReader(actual))
            {
                foreach (var ch in original)
                {
                    var act = binaryReader.ReadChar();
                    act.ShouldBe(ch);
                }
                var end = binaryReader.ReadChar();
                end.ShouldBe('\n');
                binaryReader.BaseStream.Position.ShouldBe(binaryReader.BaseStream.Length);
            }
        }

        private StreamGenerator CreateInstance()
        {
            return new StreamGenerator(readerFactory.Object);
        }
    }
}