﻿using Microsoft.Practices.Prism.UnityExtensions;
using PackageCalculations.View;
using PackageCalculations.ViewModel;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using Microsoft.Practices.Unity;

namespace PackageCalculations.Prism
{
    using PackageCalculations.Model.Prism;

    [ExcludeFromCodeCoverage]
    internal sealed class Bootstrapper : UnityBootstrapper
    {
        private Window shell;

        protected override DependencyObject CreateShell()
        {
            shell = new MainWindow();
            return shell;
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();

            shell.DataContext = Container.Resolve<IDefaultViewModel>();
            Application.Current.MainWindow = shell;
            Application.Current.MainWindow.Show();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            Container.AddExtension(new PackageCalculationsExtension());
            Container.AddExtension(new ModelExtension());
        }
    }
}
