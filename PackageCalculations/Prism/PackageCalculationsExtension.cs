﻿using Microsoft.Practices.Unity;

namespace PackageCalculations.Prism
{
    using PackageCalculations.Model.Services;
    using PackageCalculations.Services;
    using PackageCalculations.ViewModel;
    using ISelectFileService = PackageCalculations.Services.ISelectFileService;

    internal sealed class PackageCalculationsExtension : UnityContainerExtension
    {

        protected override void Initialize()
        {
            // TODO: register dependencies
            Container.RegisterType<IDefaultViewModel, DefaultViewModel>();
            Container.RegisterType<ISelectFileService, SelectFileService>();
        }
    }
}
