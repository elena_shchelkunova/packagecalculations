﻿namespace PackageCalculations.Services
{
    public interface ISelectFileService
    {
        string SelectFile();
    }
}