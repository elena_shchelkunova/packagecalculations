﻿namespace PackageCalculations.Services
{
    using Microsoft.Win32;

    public class SelectFileService : ISelectFileService
    {
        public string SelectFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            return openFileDialog.ShowDialog() == true ? openFileDialog.FileName : string.Empty;
        }
    }
}