﻿using Microsoft.Practices.Prism.Commands;
using System.ComponentModel;
using System.Windows.Input;

namespace PackageCalculations.ViewModel
{
    using System;
    using System.IO;
    using System.Reactive.Concurrency;
    using System.Reactive.Linq;
    using System.Threading;
    using PackageCalculations.Model;
    using PackageCalculations.Model.Services;
    using PackageCalculations.Model.Statistics;
    using PackageCalculations.Services;
    using System.Reactive;
    using System.Reactive.Subjects;

    internal sealed class DefaultViewModel : IDefaultViewModel
    {
        private readonly IFactory<string, IScheduler, IStatisticsModel, IFileParser> fileParserFactory;
        private readonly ISelectFileService selectFileService;
        private readonly IFactory<IStatisticsModel> statisticsModelFactory;
        private bool isRunning;
        private int progress;
        private const string TimeSpanFormaatString = @"hh\:mm\:ss\.fff";

        public string RunningStop { get { return isRunning ? "Остановить расчет" : "Запустить расчет"; } }
        public string TotalCalculationTime { get { return string.Format("Время работы: {0}", totalCalculationTime.ToString(TimeSpanFormaatString)); } }
        public string TotalCalculatedExpressions { get { return string.Format("Обработано {0} выражений", totalCalculatedExpressions); } }
        public string ExpectedRestTime { get { return string.Format("Осталось {0}", expectedRestTime.ToString(TimeSpanFormaatString)); } }
        private string filepath = @"D:\Projs\packagecalculations\data\5.txt";
        public string FilePath
        {
            get { return filepath; }
            set
            {
                filepath = value;
                RaisePropertyChanged("FilePath");
            }
        }

        private int totalCalculatedExpressions;
        private TimeSpan totalCalculationTime;
        private TimeSpan expectedRestTime;

        public bool CanRun { get { return filepath != null && File.Exists(filepath); } }
        IDisposable subscription;

        public bool IsRunnung
        {
            get { return isRunning; }
            private set
            {
                isRunning = value;
                RaisePropertyChanged("IsRunnung");
                RaisePropertyChanged("RunningStop");
            }
        }
        private readonly ICommand runStopCommand;
        private readonly BindingList<IStatisticDataRowViewModel> statistics;
        private IFileParser fileParser;
        private readonly ICommand selectFileCommand;
        
        public event PropertyChangedEventHandler PropertyChanged;

        public DefaultViewModel(IFactory<string, IScheduler, IStatisticsModel, IFileParser> fileParserFactory, 
            ISelectFileService selectFileService,
            IFactory<IStatisticsModel> statisticsModelFactory)
        {
            this.fileParserFactory = fileParserFactory;
            this.selectFileService = selectFileService;
            this.statisticsModelFactory = statisticsModelFactory;
            runStopCommand = new DelegateCommand(InvertRunning);
            selectFileCommand = new DelegateCommand(SelectFile);
            statistics = new BindingList<IStatisticDataRowViewModel>();
        }

        private void InvertRunning()
        {
            if (!isRunning)
            {
                IsRunnung = true;
                var scheduler = new SynchronizationContextScheduler(SynchronizationContext.Current);
                var taskScheduler = Scheduler.Default;
                var statisticsModel = statisticsModelFactory.Create();
                fileParser = fileParserFactory.Create(filepath, taskScheduler, statisticsModel);
                subscription = fileParser.ProgressChanged.ObserveOn(scheduler).Subscribe(FileParserProgressChanged, FileParserCompleted);
                statistics.Clear();
                ISubject<Unit> refresher = new Subject<Unit>();
                foreach (var model in statisticsModel.GetOperationsStatistics())
                    statistics.Add(new StaticticDataRowViewModel(model, refresher));

                fileParser.Parse();
            }
            else
            {
                fileParser.Cancel();
                fileParser.Dispose();
            }
        }

        private void SelectFile()
        {
            var newFile = selectFileService.SelectFile();
            if (!string.IsNullOrEmpty(newFile))
                FilePath = newFile;
        }

        private void FileParserProgressChanged(IPreogressInfoModel newProgress)
        {
            Progress = newProgress.Progress;
            totalCalculatedExpressions = newProgress.ExpressionsDone;
            totalCalculationTime = newProgress.TotalTime;
            expectedRestTime = newProgress.ExprctedRemainingTime;
            RaisePropertyChanged("TotalCalculationTime");
            RaisePropertyChanged("TotalCalculatedExpressions");
            RaisePropertyChanged("ExpectedRestTime");
        }

        private void FileParserCompleted()
        {
            IsRunnung = false;
            subscription.Dispose();
            fileParser.Dispose();
        }

        public ICommand RunStopCommand
        {
            get { return runStopCommand; }
        }

        public ICommand SelectFileCommand
        {
            get { return selectFileCommand; }
        }

        public int Progress
        {
            get { return progress; }
            set
            {
                progress = value;
                RaisePropertyChanged("Progress");
            }
        }

        public BindingList<IStatisticDataRowViewModel> Statistics
        { 
            get 
            { 
                return statistics; 
            } 
        }

        private void RaisePropertyChanged(string p)
        {
            var tmp = PropertyChanged;
            if (tmp != null)
                tmp(this, new PropertyChangedEventArgs(p));
        }
    }
}
