namespace PackageCalculations.ViewModel
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Input;

    internal interface IDefaultViewModel : INotifyPropertyChanged
    {
        bool IsRunnung { get; }
        ICommand RunStopCommand { get; }
        ICommand SelectFileCommand { get; }
        BindingList<IStatisticDataRowViewModel> Statistics { get; }
        string RunningStop { get; }
        string TotalCalculationTime { get; }
        string TotalCalculatedExpressions { get; }
        string ExpectedRestTime { get; }
        string FilePath {  set; }
        bool CanRun { get; }
    }
}