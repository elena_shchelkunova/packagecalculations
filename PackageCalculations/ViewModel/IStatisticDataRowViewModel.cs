﻿using System;

namespace PackageCalculations.ViewModel
{
    using System.ComponentModel;
    public interface IStatisticDataRowViewModel : INotifyPropertyChanged
    {
        string Operation { get; }
        int CalculationsCount { get; }
        double TotalCalculationTime { get; }
        double AverageCalculationTime { get; }
        double PercentOfTotalCalculationTime { get; }
    }
}
