﻿
namespace PackageCalculations.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using PackageCalculations.Model.Annotations;
    using PackageCalculations.Model.Statistics;
using System.Reactive.Subjects;
using System.Reactive;

    internal sealed class StaticticDataRowViewModel : IStatisticDataRowViewModel
    {
        private readonly string operation;
        private int operationsCount;
        private double duration;
        private double averageDuration;
        private Func<double> percentOfAll;
        private ISubject<Unit> refresher;

        public StaticticDataRowViewModel(IStatisticsOperationModel model, ISubject<Unit> refresher)
        {
            operation = model.OperationName;
            model.Statistics.Subscribe(OnChanged);
            this.refresher = refresher;
            refresher.Subscribe(Refresh);
        }

        private void OnChanged(IStatisticsOperationUnit data)
        {
            operationsCount = data.OperationsCount;
            duration = data.Duration;
            averageDuration = data.AverageDuration;
            percentOfAll = data.PercentOfAll;

            OnPropertyChanged("AverageCalculationTime");
            OnPropertyChanged("CalculationsCount");
            OnPropertyChanged("PercentOfTotalCalculationTime");
            OnPropertyChanged("TotalCalculationTime");
            refresher.OnNext(Unit.Default);
        }

        private void Refresh(Unit any)
        {
            OnPropertyChanged("PercentOfTotalCalculationTime");
        }

        public string Operation
        {
            get { return operation; }
        }

        public int CalculationsCount
        {
            get { return operationsCount; }
        }

        public double TotalCalculationTime
        {
            get { return duration; }
        }

        public double AverageCalculationTime
        {
            get { return averageDuration; }
        }

        public double PercentOfTotalCalculationTime
        {
            get { return percentOfAll == null ? 0.0 : percentOfAll.Invoke(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) 
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
