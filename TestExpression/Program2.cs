﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestExpression
{
    class Program2
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine() + '\n';
            int? result;
            try
            {
                using (var reader = new BinaryReader(GenerateStreamFromString(s)))
                {
                    result = ParseExpression(reader, '\n');
                }
                Console.WriteLine("result = {0}", result);
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
            Console.ReadKey();
        }

        static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        const char NullOperation = '\0';
        private static int? ParseExpression(BinaryReader reader, char end)
        {
            int? curNumber = null, prevNumber = null;
            char operation = NullOperation;
            Stack<ValueType> previousLowPriorityOperations = new Stack<ValueType>();
            char ch;
            try
            {
                do
                {
                    ch = reader.ReadChar();
                    switch (ch)
                    {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            if (curNumber.HasValue)
                                throw new Exception("Syntax error. Operator is missing");

                            curNumber = (int)Char.GetNumericValue(ch);

                            while (Char.IsNumber((char)reader.PeekChar()))
                            {
                                ch = reader.ReadChar();
                                curNumber = curNumber.Value * 10 + (int)Char.GetNumericValue(ch);
                            }

                            if (!prevNumber.HasValue && operation == '-')
                            {
                                curNumber = -curNumber;
                                operation = NullOperation;
                            }
                            break;

                        case '(':
                            curNumber = ParseExpression(reader, ')');
                            if (!prevNumber.HasValue && operation == '-')
                            {
                                curNumber = -curNumber;
                                operation = NullOperation;
                            }
                            break;

                        case '+':
                        case '-':
                        case '/':
                        case '*':
                            bool handled = false;
                            while (IsPriorityLower(operation, ch))
                            {
                                prevNumber = ExecuteOperation(operation, prevNumber, curNumber);
                                handled = true;
                                if (!previousLowPriorityOperations.Any())
                                    break;

                                curNumber = prevNumber;
                                operation = (char)previousLowPriorityOperations.Pop();
                                prevNumber = (int)previousLowPriorityOperations.Pop();
                            }

                            if (operation == NullOperation)
                            {
                                prevNumber = curNumber;
                                handled = true;
                            }
                                
                            if (!handled && prevNumber.HasValue)
                            {
                                previousLowPriorityOperations.Push(prevNumber);
                                previousLowPriorityOperations.Push(operation);
                                prevNumber = curNumber;
                            }

                            operation = ch;
                            curNumber = null;
                            break;

                        case ' ':
                            break;

                        default:
                            if (!ch.Equals(end))
                                throw new Exception(string.Format("Operation is not supported {0}", ch));

                            prevNumber = ExecuteOperation(operation, prevNumber, curNumber);
                            while (previousLowPriorityOperations.Any())
                            {
                                curNumber = prevNumber;
                                operation = (char)previousLowPriorityOperations.Pop();
                                prevNumber = (int?)previousLowPriorityOperations.Pop();
                                prevNumber = ExecuteOperation(operation, prevNumber, curNumber);
                            }
                            break;
                    }

                } while (!ch.Equals(end));
            }
            catch(Exception e)
            {
                throw new Exception(string.Format("{0} at position {1}", e.Message, reader.BaseStream.Position));
            }

            return prevNumber;
        }

        private static bool IsPriorityLower(char operation, char nextOperation)
        {
            if (operation == NullOperation)
                return false;

            if (nextOperation == '+' || nextOperation == '-')
                return true;

            if (operation == '*' || operation == '/')
                return true;

            return false;
        }

        private static int? ExecuteOperation(char operation, int? left, int? right)
        {
            if (operation == NullOperation)
                return right;

            if (!left.HasValue || !right.HasValue)
                throw new Exception("invalid syntax. Operand not found");

            switch (operation)
            {
                case '+':
                    return left + right;

                case '-':
                    return left - right;

                case '/':
                    return left / right;

                case '*':
                    return left * right;

                default:
                    throw new Exception(string.Format("Operation is not supported {0}", operation));
            }
        }
    }
}
